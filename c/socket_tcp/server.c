#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h> // exit()
#include <string.h> // memset()

#define IP   "127.0.0.1"
#define PORT 9999

int                m_socket;
int                m_child_socket;
struct addrinfo *  mp_res;
struct sockaddr_in m_address;
struct sockaddr_in m_child_address;
char               m_buffer[6];

void socket_create(void)
{
	m_socket = socket(mp_res->ai_family, mp_res->ai_socktype, mp_res->ai_protocol);
	if (m_socket == -1)
	{
		printf("ERROR: Could not create socket\n");
		exit(1);
	}
	else
	{
		printf(">>> Created socket\n");
	}
}

void socket_close(void)
{
	printf(">>> Socket closed\n");
	close(m_socket);	
}

void socket_bind(void)
{
	int retval = bind(m_socket, mp_res->ai_addr, mp_res->ai_addrlen);

	if (retval == 0)
	{
		printf(">>> Bind success\n");
	}
	else
	{
		printf("ERROR: Could not bind\n");
		socket_close();
		exit(1);
	}
}



void socket_listen(void)
{
	int retval = listen(m_socket, 5);
	if (retval == -1)
	{
		printf("ERROR: Cannot listen on socket\n");
		socket_close();
		exit(1);
	}
	else
	{
		printf(">>> listening\n");
	}
}


void child_socket_accept(void)
{
	memset(&m_child_address, 0, sizeof(m_child_address));
	int size = sizeof(m_child_address);
	
	m_child_socket = accept(m_socket, (struct sockaddr *)&m_child_address, &size);

	if (m_child_socket == -1)
	{
		printf("ERROR: Could not accept connection\n");
		socket_close();
		exit(1);
	}
	else
	{
		printf(">>> accepted\n");
	}
}

void child_socket_write(void)
{
	int retval = write(m_child_socket, "World", 6);
	
	if (retval > 0)
	{
		printf(">>> Write data: %s\n", "World");
	}
	else
	{
		printf("ERROR: Could not write data\n");
	}
}

void child_socket_read(void)
{
	int retval = read(m_child_socket, m_buffer, sizeof(m_buffer));
	if (retval > 0)
	{
		printf(">>> Read data (%i): %s\n", retval, m_buffer);
	}
	else
	{
		printf("ERROR: Could not read data\n");
	}
}

void child_socket_close(void)
{
	close(m_child_socket);
}

void socket_resolve_ai_family(int argc, char * argv[])
{
	struct addrinfo hints;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = PF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	int retval = getaddrinfo(argv[1], argv[2], &hints, &mp_res);
	if (retval)
	{
		printf("getaddrinfo: %s\n", gai_strerror(retval));
		exit(1);
	}
}
 
int main(int argc, char * argv[])
{
	if (argc != 3)
	{
		printf("Usage: %s <server> <port>\n", argv[0]);
		exit(1);
	}
	
	socket_resolve_ai_family(argc, argv);

	socket_create();
	socket_bind();
	socket_listen();
	while (1)
	{
		child_socket_accept();
		child_socket_read();
		child_socket_write();
		child_socket_close();
		break;
	}

	socket_close();
	freeaddrinfo(mp_res);

	return 0;
}