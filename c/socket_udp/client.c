#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h> // exit()
#include <string.h> // memset()

int                m_socket;
char               m_buffer[6];
struct addrinfo *  mp_res;



void socket_create(void)
{
	m_socket = socket(mp_res->ai_family, mp_res->ai_socktype, mp_res->ai_protocol);
	if (m_socket == -1)
	{
		printf("ERROR: Could not create socket\n");
		exit(1);
	}
	else
	{
		printf(">>> Created socket\n");
	}
}

void socket_close(void)
{
	printf(">>> Socket closed\n");
	close(m_socket);
}

void socket_bind(void)
{
	int retval = connect(m_socket, mp_res->ai_addr, mp_res->ai_addrlen);

	if (retval == 0)
	{
		printf(">>> Bind complete\n");
	}
	else
	{
		printf("ERROR: Could not bind to address\n");
		socket_close();
		exit(1);
	}
}

void socket_sendto(void)
{
	int retval = sendto(m_socket, "Hello", 6, 0, (struct sockaddr *)&mp_res->ai_addr, mp_res->ai_addrlen);
	if (retval > 0)
	{
		printf(">>> Sentto: %s\n", "Hello");
	}
	else
	{
		printf("ERROR: Could not sendto data\n");
	}
}

void socket_recvfrom(void)
{
	int size = mp_res->ai_addrlen;

	int retval = recvfrom(m_socket, m_buffer, sizeof(m_buffer), 0, (struct sockaddr *)&mp_res->ai_addr, &size);
	if (retval > 0)
	{
		printf(">>> Readfrom data (%i): %s\n", retval, m_buffer);
	}
	else
	{
		printf("ERROR: Could not readfrom\n");
	}
}

void socket_resolve_ai_family(int argc, char * argv[])
{
	struct addrinfo hints;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = PF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_protocol = IPPROTO_UDP;

	int retval = getaddrinfo(argv[1], argv[2], &hints, &mp_res);
	if (retval)
	{
		printf("getaddrinfo: %s\n", gai_strerror(retval));
		exit(1);
	}
}

int main(int argc, char * argv[])
{
	if (argc != 3)
	{
		printf("Usage: %s <server> <port>\n", argv[0]);
		exit(1);
	}
	
	socket_resolve_ai_family(argc, argv);

	socket_create();
	socket_bind();
	socket_sendto();
	socket_recvfrom();
	socket_close();

	freeaddrinfo(mp_res);

	return 0;
}